package com.sda.db;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Mockito.*;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class DbTestMock {

    @Mock
    private Database dbMock;

    @Test
    public void testFindUser(){
        when(dbMock.findUser(anyString())).thenReturn(new User("login", "pass"));
        when(dbMock.findUser("Marek")).thenReturn(new User("Marek", "pass2"));

        User mockedUser1 = dbMock.findUser("blabla");
        User mockedUser2 = dbMock.findUser("Marek");

        assertThat(mockedUser1.getLogin()).isEqualTo("login");
        assertThat(mockedUser2.getLogin()).isEqualTo("Marek");

    }


}
